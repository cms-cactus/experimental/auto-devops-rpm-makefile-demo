#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <string>
#include <sstream>
#include <time.h>

#define PORT 8080
#define BUFSIZE 1

int server_fd;
sockaddr *p_address;
socklen_t addrlen;
int socket_fd;
void getSocket() {
    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (!server_fd) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    int opt = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt2");
        exit(EXIT_FAILURE);
    }
    struct sockaddr_in address;
    p_address = (struct sockaddr *)&address;
    addrlen = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, p_address, addrlen) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
}

void write(const char *msg) {
    send(socket_fd , msg , strlen(msg) , 0);
}
void writeln(const char *msg) {
    write(msg);
    write("\n");
}

int serve() {
    socket_fd = accept(server_fd, p_address, &addrlen);
    if (socket_fd < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    fd_set rfds;
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    FD_ZERO(&rfds);
    FD_SET(socket_fd, &rfds);
    char buffer[BUFSIZE] = {0};
    while (select(socket_fd + 1, &rfds, NULL, NULL, &tv)) {
        read(socket_fd, buffer, BUFSIZE);
    }

    const char *hostname = getenv("HOSTNAME");
    if (hostname == NULL) {
        hostname = "?";
    }
    const char *secret = getenv("DEMOSECRET");
    if (secret == NULL) {
        secret = "?";
    }
    writeln("HTTP/1.1 200 OK\n");
    writeln("<head>");
    writeln("  <title>rpm-demo-app</title>");
    writeln("</head>");
    writeln("<body>");
    write("  <p>I am running on ");
    write(hostname);
    writeln("</p>");
    writeln("  <p>K8S_SECRET_DEMOSECRET at the time of deployment was \"");
    write(secret);
    writeln("\"</p>");
    writeln("</body>");
    writeln("</html>");
    close(socket_fd);
    return 0;
}

int main() {
    getSocket();
    while (true) {
        int r = serve();
        if (r != 0) {
            return r;
        }
    }
}