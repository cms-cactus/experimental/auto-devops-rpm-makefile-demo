package internal

import (
	"fmt"
	"net/http"
	"os"
)

var responsestr = `<html>
<head>
  <title>rpm-demo-app</title>
</head>
<body>
  <p>I am running on %s</p>
  <p>K8S_SECRET_DEMOSECRET at the time of deployment was "%s"</p>
  <p>I have been running for about %s</p>
</body>
</html>
`

// GetHandler generates a handler with given timer
func GetHandler(timer Timer) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, responsestr,
			os.Getenv("HOSTNAME"),
			os.Getenv("DEMOSECRET"),
			timer.report(),
		)
	}
}
