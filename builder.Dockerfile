ARG BASE_OS=cc7
ARG BASE_TAG
FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/builder-makefile:${BASE_OS}-${BASE_TAG}
LABEL maintainer="Cactus <cactus@cern.ch>"

ENV GO_VERSION=1.14
ENV PATH="$PATH:/usr/local/go/bin:/root/go/bin"

RUN curl -o go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz && \
    go get -u golang.org/x/tools/...
