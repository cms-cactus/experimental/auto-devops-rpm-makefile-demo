SHELL:=/bin/bash
.DEFAULT_GOAL := build

build:
	mkdir -p build
	go build -o build/serve ./cmd/serve.go

.PHONY: clean
clean:
	rm -rf build

test:
	mkdir -p build/coverage
	go test ./internal -covermode=count -coverprofile=build/coverage/report.out
	go tool cover -html=build/coverage/report.out -o build/coverage/index.html

.PHONY: rpm
rpm: build
	if git describe --tags --abbrev=0 > gittag; then echo; fi;
	if [[ -z "$$(cat gittag)" ]]; then git rev-parse --abbrev-ref HEAD > gittag; fi
	echo $$(($$(git rev-list --count $$(cat gittag)..HEAD) +1)) > rev
	fpm \
		-s dir \
		-t rpm \
		-n demo-server \
		-v "$$(cat gittag)" \
		-m "<cactus@cern.ch>" \
		--iteration "$$(cat rev)" \
		--vendor CERN \
		--description "demo server" \
		--provides demo-server \
		./build/=/opt/cactus/demoserver \
		./systemd/=/usr/lib/systemd/system
	mkdir -p rpms
	mv *.rpm rpms
	rm -rf gittag rev