package main

import (
	"demo-server/internal"
	"fmt"
	"net/http"
	"os"
)

func main() {
	var port = "80"
	if len(os.Args) > 1 {
		port = os.Args[1]
	}
	address, err := internal.ParsePort(port)
	checkErr(err, "invalid port "+port)

	checkErr(serve(address, internal.MakeRuntimeTimer()), "cannot start server")
}

func serve(address internal.Address, timer internal.Timer) error {
	http.HandleFunc("/", internal.GetHandler(timer))
	fmt.Println("listening on", address)
	return http.ListenAndServe(string(address), nil)
}

func checkErr(err error, message string) {
	if err != nil {
		fmt.Fprintln(os.Stderr, message, err)
		os.Exit(1)
	}
}
