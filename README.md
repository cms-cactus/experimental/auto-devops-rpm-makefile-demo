[![pipeline status](https://gitlab.cern.ch/cms-cactus/experimental/auto-devops-rpm-makefile-demo/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-cactus/experimental/auto-devops-rpm-makefile-demo/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cms-cactus/experimental/auto-devops-rpm-makefile-demo/badges/master/coverage.svg?job=test)](https://gitlab.cern.ch/cms-cactus/experimental/auto-devops-rpm-makefile-demo/-/commits/master)

## GitLab Cactus AutoDevops project demo

example markdown notation [here](doc/dummy.md)

![img](test.jpg)